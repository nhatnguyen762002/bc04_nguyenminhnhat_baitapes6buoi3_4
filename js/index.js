import {
  renderCompletedList,
  renderTodoList,
} from "./controller/todo_controller.js";

let todoList = ["Nhắn tin cho ngừi eo", "Đọc sách", "Sửa xe"];
let completedList = [];

renderTodoList(todoList);

document.getElementById("addItem").addEventListener("click", () => {
  let taskValue = document.getElementById("newTask").value;

  if (taskValue != "") {
    todoList.push(taskValue);
    document.getElementById("newTask").value = ``;
  }

  renderTodoList(todoList);
});

let xoaTask = (task, list) => {
  if (list == "todoList") {
    let indexTask = todoList.indexOf(task);

    if (indexTask != -1) {
      todoList.splice(indexTask, 1);
      renderTodoList(todoList);
    }
  } else {
    let indexTask = completedList.indexOf(task);

    if (indexTask != -1) {
      completedList.splice(indexTask, 1);
      renderCompletedList(completedList);
    }
  }
};
window.xoaTask = xoaTask;

let addCompletedTask = (task) => {
  let indexCompletedTask = todoList.indexOf(task);

  if (indexCompletedTask != -1) {
    completedList.push(todoList[indexCompletedTask]);
    xoaTask(task, "todoList");

    renderTodoList(todoList);
    renderCompletedList(completedList);
  }
};
window.addCompletedTask = addCompletedTask;

let removeCompletedTask = (task) => {
  let indexTask = completedList.indexOf(task);

  if (indexTask != -1) {
    todoList.push(completedList[indexTask]);
    xoaTask(task, "completedList");

    renderTodoList(todoList);
    renderCompletedList(completedList);
  }
};
window.removeCompletedTask = removeCompletedTask;

document.getElementById("two").addEventListener("click", () => {
  todoList.sort((a, b) => a.localeCompare(b));
  renderTodoList(todoList);
});

document.getElementById("three").addEventListener("click", () => {
  todoList.sort((a, b) => b.localeCompare(a));
  renderTodoList(todoList);
});
