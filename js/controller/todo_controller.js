export let renderTodoList = (todoList) => {
  let todoListEl = document.getElementById("todo");
  todoListEl.innerHTML = ``;

  todoList.forEach((task) => {
    todoListEl.innerHTML += `
    <li>
        ${task}
        <div>
            <i onclick="xoaTask('${task}', 'todoList')" style="cursor: pointer" class="fas fa-trash-alt"></i>
            <i onclick="addCompletedTask('${task}')" style="cursor: pointer" class="far fa-check-circle"></i>
        </div>
        </li>
    `;
  });
};

export let renderCompletedList = (completedList) => {
  let completedListEl = document.getElementById("completed");
  completedListEl.innerHTML = ``;

  completedList.forEach((task) => {
    completedListEl.innerHTML += `  
      <li>
          ${task}
          <div>
              <i onclick="xoaTask('${task}', 'completedList')" style="cursor: pointer" class="fas fa-trash-alt"></i>
              <i onclick="removeCompletedTask('${task}')" style="cursor: pointer; color: rgb(106, 212, 0)" class="fas fa-check-circle"></i>
          </div>
          </li>
      `;
  });
};
